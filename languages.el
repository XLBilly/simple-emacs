(use-package plantuml-mode
  :ensure t
  :defer t
  :pin melpa-stable
  :config
  (setq plantuml-jar-path "~/.emacs.d/others/plantuml.jar")
  (setq plantuml-exec-mode 'jar))
;;(use-package yaml-mode
;;  :ensure t)
(use-package dashboard
  :ensure t
  :pin melpa-stable
  :config
  (setq dashboard-items '((recents . 5)
	(bookmarks . 5)
	(registers . 5)))
  (dashboard-setup-startup-hook))
(use-package solarized-theme
	     :ensure t)
(use-package ox-gfm
  :after ox
  :defer t
  :ensure t)
(use-package ox-asciidoc
  :after ox
  :defer t
  :ensure t)
(use-package adoc-mode
  :ensure t
  :defer t)
(use-package denote
  :ensure t)
(use-package org
  :defer t
  :init
  (load "~/.emacs.d/org-init.el")
  
  :config
  (load "~/.emacs.d/org-config.el"))
