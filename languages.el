(use-package plantuml-mode
  :ensure t
  :config
  (setq plantuml-jar-path "~/.emacs.d/others/plantuml.jar")
  (setq plantuml-exec-mode 'jar))
(use-package yaml-mode
  :ensure t)
(use-package solarized-theme
	     :ensure t)
(use-package ox-gfm
  :after ox
  :ensure t)
